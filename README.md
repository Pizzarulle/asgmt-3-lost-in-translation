# Asgmt 3:Lost in Translation

Simple React application where you can login, translate english (none numerical) words and then see the translation history under the profile page 

## Table of Contents

- [Component Tree](#componentTree)
- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)

## Component Tree
<a href="Asgmt3-Lost-in-translation-Component-Tree.png"> <img src="Asgmt3-Lost-in-translation-Component-Tree.png" width="850" /> </a>

## Install

In the root of the folder open a terminal or powershell window and run:

```sh
npm install
```

## Usage

Open a terminal or powershell window and run:

```sh
npm start
```

Further instructions will appear in your console. Leave the window open while in use.

## Maintainers
[@Andreas Hellström](https://www.gitlab.com/pizzarulle) [@Rickard Cruz](https://www.gitlab.com/cruz120)
