import { API_KEY } from "../constants/constants"

export const createHeaders = () => {
    return {
        'Content-Type': 'application/json',
        'x-api-key': API_KEY
    }
}