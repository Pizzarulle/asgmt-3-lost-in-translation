import { createHeaders } from "."
import { API_URL } from "../constants/constants"


/**
 * Check if there is an existing user with that username. Always returns an array containing two values [protential error, portenital user]
 * @param {string} string - username 
 * @returns [value, array]
 */
const userCheckFor = async (username) => {
    try {
        const response = await fetch(`${API_URL}?username=${username}`)
        if (!response.ok) {
            throw new Error("Could not check for a user with username: " + username)
        }
        const data = await response.json()
        return [null, data]
    } catch (error) {
        return [error.message, []]
    }
}


/**
 * Uses POST method to create a user. Always returns an array containing two values [protential error, portenital user]
 * @param {string} string - New username 
 * @returns [value, array] 
 */
const userCreate = async (username) => {
    try {
        const response = await fetch(API_URL, {
            method: 'POST',
            headers: createHeaders(),
            body: JSON.stringify({
                username,
                translations: []
            })
        })
        if (!response.ok) {
            throw new Error("Could not create user with username: " + username)
        }
        const data = await response.json()
        return [null, data]
    } catch (error) {
        return [error.message, []]
    }
}

/**
 * Uses PATCH to update a users translations by its id. Always returns an array containing two values [protential error, portenital user]
 * @param {*} userId - Users id
 * @param {Array} Array - Updated translations 
 * @returns [value, array] 
 */
export const userUpdate = async (userId, translations) => {
    try {
        const response = await fetch(`${API_URL}/${userId}`, {
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                translations
            })
        })
        if (!response.ok) {
            throw new Error("Could not update user!")
        }

        const data = await response.json()
        return [null, data]
    } catch (error) {
        return [error.message, null]
    }
}

/**
 * Check if user exists. If it does it returns the user from the api. If theres an error it returns the error message. If there is no user it creates a new user
 * @param {string} string - Username 
 * @returns [value, array] 
 */
export const userLogin = async (username) => {
    const [potentialError, user] = await userCheckFor(username)

    if (potentialError !== null) {
        return [potentialError, null]
    }

    if (user.length > 0) {
        return [null, user.pop()]
    }
    return await userCreate(username)
}
