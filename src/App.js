import { BrowserRouter, Route, Routes } from "react-router-dom";
import Navbar from "./components/Navbar/Navbar";
import Login from "./views/Login";
import Profile from "./views/Profile";
import Translation from "./views/Translation";

function App() {
  return (
    <BrowserRouter basename={process.env.NODE_ENV === 'production' ? '/asgmt-3-lost-in-translation/' : '/'}>
      <div>
        <Navbar />
        <Routes>
          <Route path="/" element={<Login />} />
          <Route path="/translation" element={<Translation />} />
          <Route path="/profile" element={<Profile />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
