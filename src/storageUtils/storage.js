
/**
 * Set item in sessionStorage
 * @param {string} key - String 
 * @param {*} value - any
 */
export const storageSet = (key, value) => {
    sessionStorage.setItem(key, JSON.stringify(value))
}
/**
 * Read item from sessionStorage
 * @param {string} key - String 
 * @returns Json parsed value
 */
export const storageGet = (key) => {
    const item = sessionStorage.getItem(key)

    return JSON.parse(item)
}

/**
 * Delete item from sessionStorage
 * @param {string} key  - String
 */
export const storageDelete = (key) => {
    sessionStorage.removeItem(key)
}