import { createContext, useContext, useState } from "react"
import { STORAGE_KEY_USER } from "../constants/constants"
import { storageGet } from "../storageUtils/storage"

const UserContext = createContext()

export const useUser = () => {
    return useContext(UserContext)
}

const UserProvider = ({ children }) => {

    //Retrives user from sessionStorage. If sessionStorage dosnt have a user it returns null
    const [user, setUser] = useState(storageGet(STORAGE_KEY_USER))
    const state = {
        user,
        setUser
    }
    return (
        <UserContext.Provider value={state}>
            {children}
        </UserContext.Provider>
    )
}
export default UserProvider