import ProfileHeader from "../components/Profile/ProfileHeader"
import ProfileTranslationHistory from "../components/Profile/ProfileTranslationHistory"
import { useUser } from "../context/UserContext"
import withAuth from "../hoc/withAuth"

const Profile = () => {
    const {user} = useUser()
    return (
        <div>
            <ProfileHeader username={user.username}/>
            <ProfileTranslationHistory/>
        </div>
    )
}
export default withAuth(Profile)
