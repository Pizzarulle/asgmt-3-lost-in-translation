import LoginForm from "../components/Login/LoginForm"
import "../styling/loginView/login.css"

const Login = () => {
    return (
        <div className="login-container">
            <div className="welcome-container">
                <img src="images/Logo-Hello.png" alt="Logo-Hello" width={350} />
                <div>
                    <h1>Lost in Translation</h1>
                    <h3>Get started</h3>
                </div>
            </div>

            <LoginForm />
        </div>
    )
}
export default Login
