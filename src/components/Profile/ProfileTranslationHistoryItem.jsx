const ProfileTranslationHistoryItem = ({ translations }) => {

    //returns a list containing every element that hasnt the "deleted" property set to true
    const translationList = translations.filter((translation) => !translation.deleted).reverse();

    /**
     *  Uses the values in translationList to represent max 10 elements
     * 
     *  @returns {Array} array - Array containing max 10 elements
     */
    const tenItems = (() => {
        let maxTenItemsArray = [...translationList]

        while(maxTenItemsArray.length > 10){
            maxTenItemsArray.pop()
        }
        return maxTenItemsArray.map((translation, index) => {
            return <div className="history-item" key={index + "-" + translation}>
                <h4>{translation}</h4>
            </div>
        })
    })()

    return (
        <>
            {tenItems}
            {tenItems.length === 0 && <span className="history-item">You have no visable translations! Please go and translate something.</span>}
        </>
    );
}

export default ProfileTranslationHistoryItem;