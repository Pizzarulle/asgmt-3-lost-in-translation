const ProfileHeader = ({ username }) => {
    return (
        <header className="welcome-container">
            <img src="images/Logo.png" alt="Logo" width={300} />
            <div>
                <h1>Hello, {username} !</h1>
                <h4>Here you can look at your previous translations and also remove them!</h4>
            </div>
        </header>
    );
}

export default ProfileHeader;
