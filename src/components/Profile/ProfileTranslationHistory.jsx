import { userUpdate } from "../../api/user"
import { useUser } from "../../context/UserContext"
import { STORAGE_KEY_USER } from "../../constants/constants"
import { storageSet } from "../../storageUtils/storage"

import "../../styling/profile/profile.css"
import ProfileTranslationHistoryItem from "./ProfileTranslationHistoryItem"

const ProfileTranslationHistory = () => {

    const { user, setUser } = useUser()

    /**
     * Converts the the 10 last array elements to induvidial objects with the properties "deleted: true" and text: "". 
     * If one of the 10 items already is an object it continues to the next element without doing anyting. User is updated localay and remote
     */
    const onClickDeleteTranslations = async () => {
        try {
            if (!window.confirm("This will delete the 10 most recent translations! Are you sure?")) {
                return
            }
            const currentTranslations = [...user.translations].reverse()

            let i = 0
            const modifiedTranlstionsList = currentTranslations.map(element => {
                if (i < 10) {
                    if (!element.deleted) {
                        i++
                        return { deleted: true, text: element }
                    }
                }
                return element
            }).reverse()

            const [error] = await userUpdate(user.id, modifiedTranlstionsList)

            if (error !== null) {
                throw new Error(error)
            }

            const updatedUser = {
                ...user,
                translations: modifiedTranlstionsList
            }
            storageSet(STORAGE_KEY_USER, updatedUser)
            setUser(updatedUser)

        } catch (error) {
            alert(error.message)
        }
    }

    return (
        <div className="translation-history-container">
            <div className="translation-history-title">
                <b>Translation History</b>
                <button className="delete-translatios-btn" onClick={onClickDeleteTranslations}>Delete</button>
            </div>
            <div className="translation-history">
                <ProfileTranslationHistoryItem translations={user.translations} />
            </div>
            <div className="buttom-border-div"></div>
        </div>
    )
}

export default ProfileTranslationHistory;