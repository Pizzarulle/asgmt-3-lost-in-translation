import { NavLink, useLocation } from "react-router-dom";
import { STORAGE_KEY_USER } from "../../constants/constants"
import { useUser } from "../../context/UserContext"
import { storageDelete } from "../../storageUtils/storage"
import "../../styling/navbar/navbar.css"

const NavbarUserIcon = ({ username }) => {

    const { pathname } = useLocation();
    const { setUser } = useUser()

    /**
     * Removes the local user from session storage
     */
    const onClickLogout = () => {
        if (window.confirm("Are you sure?")) {
            storageDelete(STORAGE_KEY_USER)
            setUser(null)
        }
    }
    // Displays different components depending on the current pathname
    const locationComponent = (() => {
        if (pathname === "/profile") {
            return <>
                <NavLink to="/translation" className="navlink">Translate</NavLink>
                <button className="logout-btn" onClick={onClickLogout}>Logout</button>
            </>
        }
        return (
            <div>
                <NavLink className="user-icon-text" to="/profile">
                    {username}
                </NavLink>
                <NavLink className="user-icon" to="/profile">
                    <img src="images/UserIcon.png" alt="user-icon" width={100} />
                </NavLink>
            </div>
        )
    })()
    return (
        <>
            {locationComponent}
        </>
    );
}

export default NavbarUserIcon;
