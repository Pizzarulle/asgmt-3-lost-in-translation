import { useUser } from "../../context/UserContext";
import "../../styling/navbar/navbar.css"
import NavbarUserIcon from "./NavbarUserIcon";

const Navbar = () => {
    const { user } = useUser()
    return (
        <nav className="navbar">
            <h1>Lost in Translation</h1>
             {user && <NavbarUserIcon username={user.username} />}
        </nav>
    );
}

export default Navbar;