
import TranslationOutput from "./TranslationOutput";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { useUser } from "../../context/UserContext"
import { userUpdate } from "../../api/user"
import "../../styling/translation/translationForm.css"
import { STORAGE_KEY_USER } from "../../constants/constants";
import { storageSet } from "../../storageUtils/storage";

const URL_SIGN_IMAGES = "LostInTranslation_Resources/individial_signs/";

const TranslationForm = () => {

   const [translation, setTranslation] = useState([])
   const { register, handleSubmit, reset } = useForm();
   const { user, setUser } = useUser()
   /**
    *  Updates the translation input to the user API
    * @param {*} data contains submitted user input 
    */
   const onSubmit = async (data) => {
      setTranslation(createSigns(data.translationInput))

      const updatedTranslations = [...user.translations]
      updatedTranslations.push(data.translationInput)

      const [error] = await userUpdate(user.id, updatedTranslations)

      if (error !== null) {
         throw new Error(error)
      }

      const updatedUser = {
         ...user,
         translations: updatedTranslations
      }
      storageSet(STORAGE_KEY_USER, updatedUser)
      setUser(updatedUser)
      
      //clears the input value in the form
      reset()
   }
   
   /**
    * Generates the signs of a given string.
    * @param {*} strInput contains the user input.
    * @returns An array of sign references of the sign images.
    */
   const createSigns = (strInput) => {
      const signs = []
      let strArray = Array.from(strInput)

      for (let i = 0; i < strArray.length; i++) {
         if (strArray[i] !== " ")
            signs.push({ index: i, sign: `${URL_SIGN_IMAGES}${strArray[i]}.png`, alt: strArray[i] })
      }
      return signs;
   }

   return (
      <>
         <div className="translate-container">
            <form onSubmit={handleSubmit(onSubmit)}>
               <fieldset className="translation-fieldset">
                  <input type="text"
                     className="translation-input"
                     placeholder="Translate"
                     name="Translation"
                     {...register("translationInput", {
                        required: {
                           value: /^[A-Za-z]*$/,
                           message: 'error message'
                        }
                     })} />
                  <button className="input-btn" type="submit">
                     <img src="images/ArrowIcon.png" alt="arrow-icon" width={60} />
                  </button>
               </fieldset>
            </form>
         </div>

         <TranslationOutput translations={translation} />
      </>
   )
}

export default TranslationForm;

