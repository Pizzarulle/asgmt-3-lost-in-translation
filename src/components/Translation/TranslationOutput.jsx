import "../../styling/translation/translationOutput.css"

/**
 * 
 * @param {*} param0 translation
 * @returns the sign-translation for each letter
 */
const TranslationOutput = ({ translations }) => {
     const signs = translations.map((signs) => {
          return signs.sign !== null ? <img key={signs.index} 
                                            src={signs.sign} 
                                            alt={signs.alt}></img> : <></>
     });
     return (
          <div className="translation-result-container">
               <div className="translation-result">
                    <ul>
                         {translations.length > 0 ? signs : <></>}
                    </ul>
               </div>
               <div className="buttom-border-div"></div>
          </div>
     )
}

export default TranslationOutput;