import { useEffect, useState } from "react"
import { useForm } from "react-hook-form"
import { useNavigate } from "react-router-dom"
import { userLogin } from "../../api/user"
import { STORAGE_KEY_USER } from "../../constants/constants"
import { useUser } from "../../context/UserContext"
import { storageSet } from "../../storageUtils/storage"
import "../../styling/loginView/loginForm.css"

//form input restraints
const formInputConfig = {
    required: true,
    minLength: 3
}

const LoginForm = () => {
    const { register, handleSubmit, formState: { errors } } = useForm()
    const { user, setUser } = useUser()
    const navigate = useNavigate()

    const [isLoading, setLoading] = useState(false)

    //Navigates to profile page when user is changed
    useEffect(() => {
        if (user !== null) {
            navigate("/translation")
        }
    }, [user, navigate])

    
    /**
     * Makes request to login a user with specified username. If successes the user is stored in {@link UserContext} and saved in to {@link sessionStorage}
     * Login button is disabled so it canot be clicked multiple times while its fetching a user
     * @param {string} string - Username
     */
    const onSubmit = async ({ username }) => {
        setLoading(true)
        const [error, returnedUser] = await userLogin(username)
        if (returnedUser !== null) {
            storageSet(STORAGE_KEY_USER, returnedUser)
            setUser(returnedUser)
        }
        setLoading(false)
    }

    //returns any potential errors from form input
    const errorMessage = (() => {
        if (!errors.username) {
            return null
        }
        if (errors.username.type === "required") {
            return <span className="error-message">Username is required</span>
        }
        if (errors.username.type === "minLength") {
            return <span className="error-message">Username is too short (min 3)</span>
        }
    })()

    return (
        <div className="loginForm-container">
            <form className="login-form" onSubmit={handleSubmit(onSubmit)} >
                <fieldset className="login-fieldset" >
                        <input
                            type="text"
                            className="login-input"
                            placeholder="What is you name?"
                            {...register("username", formInputConfig)}
                        />

                    <button className="submit-btn" type="submit" disabled={isLoading}>
                        <img src="images/ArrowIcon.png" alt="arrow-icon" width={55} />
                    </button>
                </fieldset>

                {isLoading && <p>Logging in...</p>}
                {errorMessage}

                <div className="buttom-border-div"></div>
            </form>
        </div>
    )
}
export default LoginForm
